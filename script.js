$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOMBO_SMALL = "Small";
    const gCOMBO_MEDIUM = "Medium";
    const gCOMBO_LARGE = "Large";
    var gOjbOrder
    // các loại pizza
    const gPIZZA_OCEAN = "OCEAN MANIA";
    const gPIZZA_HAWAI = "HAWAIIAN";
    const gPIZZA_BACON = "CHICKEN BACON";

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // chọn size pizza
    $("#btn-small").on("click", function () {
        onBtnSmallClick()
    });
    $("#btn-medium").on("click", function () {
        onBtnMediumClick()
    });
    $("#btn-large").on("click", function () {
        onBtnLargeClick()
    });
    // chọn loại Pizza
    $("#btn-ocean").on("click", function () {
        onBtnOceanClick()
    });
    $("#btn-hawai").on("click", function () {
        onBtnHawaiClick()
    });
    $("#btn-bacon").on("click", function () {
        onBtnBaconClick()
    });
    // GỬI Đơn
    $("#btn-send").on("click", function(){
        onBtnSendClick();
    })
    // nút tạo đơn hàng
    $("#btn-create-order-modal").on("click", function(){
        onBtnCreateOrder()
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        // API lấy danh sách đồ uống, để load vào Select đồ uống
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
            type: "GET",
            success: function (paramDrink) {
                handleDrinkList(paramDrink); //hiển thị data vào drink select
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });


    }
    // hàm được gọi khi Pizza OCEAN được chọn
    function onBtnOceanClick() {
        changeTypePizzaButtonColor(gPIZZA_OCEAN)
        console.log("Pizza ", gPIZZA_OCEAN)
    }
    // hàm được gọi khi Pizza HAWAIIAN được chọn
    function onBtnHawaiClick() {
        changeTypePizzaButtonColor(gPIZZA_HAWAI)
        console.log("Pizza ", gPIZZA_HAWAI)
    }
    // hàm được gọi khi Pizza CHICKEN BACON được chọn
    function onBtnBaconClick() {
        changeTypePizzaButtonColor(gPIZZA_BACON)
        console.log("Pizza ", gPIZZA_BACON)
    }

    // hàm được gọi khi nút Combo Small 
    function onBtnSmallClick() {
        changeComboButtonColor(gCOMBO_SMALL)
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }

    // hàm được gọi khi nút Combo Medium 
    function onBtnMediumClick() {
        changeComboButtonColor(gCOMBO_MEDIUM)
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }

    // hàm được gọi khi nút Combo Large 
    function onBtnLargeClick() {
        changeComboButtonColor(gCOMBO_LARGE)
        //tạo một đối tượng menu, được tham số hóa
        var vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
        // gọi method hiển thị thông tin
        vSelectedMenuStructure.displayInConsoleLog();
    }

    //function trả lại một đối tượng combo (menu được chọn) được tham số hóa
    function getComboSelected(paramMenuName, paramDuongKinhCM, paramSuongNuong, paramSaladGr, paramDrink, paramPriceVND) {
        var vSelectedMenu = {
            menuName: paramMenuName, // S, M, L
            duongKinhCM: paramDuongKinhCM,
            suongNuong: paramSuongNuong,
            saladGr: paramSaladGr,
            drink: paramDrink,
            priceVND: paramPriceVND,
            displayInConsoleLog() {
                console.log("%cPLAN MENU COMBO - .........", "color:blue");
                console.log(this.menuName); //this = "đối tượng này"
                console.log("duongKinhCM: " + this.duongKinhCM);
                console.log("suongNuong: " + this.suongNuong);
                console.log("saladGr: " + this.saladGr);
                console.log("drink:" + this.drink);
                console.log("priceVND: " + this.priceVND);
            },
        };
        return vSelectedMenu;
    }
    // hàm khi nút gửi đơn được ấn
    function onBtnSendClick(){
      var vGetDataOrder =  getOrder()
      console.log(vGetDataOrder)
      var IsCheck = validateData(vGetDataOrder)
      if(IsCheck){
        showOrderInfoToModal(vGetDataOrder)
        $("#order-modal").modal("show")
      }
    }

    // hàm nút tạo đơn hàng được ấn
    function onBtnCreateOrder(){
        var vCeateOrder =  getOrder()
        // var vDiscount = vCeateOrder.priceAnnualVND() - (vCeateOrder.menuCombo.priceVND)
        // khai báo object order để chứa thông tin đặt hàng
        var vObjectRequest = {
            kichCo: vCeateOrder.menuCombo.menuName,
            duongKinh: vCeateOrder.menuCombo.duongKinhCM,
            suon: vCeateOrder.menuCombo.suongNuong,
            salad: vCeateOrder.menuCombo.saladGr,
            loaiPizza: vCeateOrder.loaiPizza,
            idVourcher: vCeateOrder.voucher,
            idLoaiNuocUong: vCeateOrder.drink,
            soLuongNuoc: vCeateOrder.menuCombo.drink,
            hoTen: vCeateOrder.hoVaTen,
            thanhTien: vCeateOrder.priceAnnualVND(),
            email: vCeateOrder.email,
            soDienThoai: vCeateOrder.dienThoai,
            diaChi: vCeateOrder.diaChi,
            loiNhan: vCeateOrder.loiNhan,
        }
        callApiCreateOrder(vObjectRequest);

    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // thu thập dữ liệu Order
    function getOrder(){
        var vBtnSmall = $("#btn-small").prop("data-is-selected-menu");
        var vBtnMedium = $("#btn-medium").prop("data-is-selected-menu");
        var vBtnLarge = $("#btn-large").prop("data-is-selected-menu");
        var vSelectedMenuStructure = "";
          if(vBtnSmall === "Y"){
            vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
          } 
          else if(vBtnMedium === "Y"){
            vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
          }
          else if(vBtnLarge === "Y"){
            vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
          };
          var vBtnOcean = $("#btn-ocean").prop("data-is-selected-pizza");
          var vBtnHawai = $("#btn-hawai").prop("data-is-selected-pizza");
          var vBtnBacon = $("#btn-bacon").prop("data-is-selected-pizza");
          var vLoaiPizza = "";
          if(vBtnOcean === "Y"){
            vLoaiPizza = gPIZZA_OCEAN
          } 
          else if(vBtnHawai === "Y"){
            vLoaiPizza = gPIZZA_HAWAI
          }
          else if(vBtnBacon === "Y"){
            vLoaiPizza = gPIZZA_BACON
          };
          var vSelectDrink = $("#select-drink").val()
          var vInpName = $("#inp-name").val();
          var vInpEmail = $("#inp-email").val();
          var vInpPhone = $("#inp-phone").val();
          var vInpAddress = $("#inp-address").val();
          var vInpMessage = $("#inp-message").val();
          var vInpVoucher = $("#inp-voucher").val();
          var vPercent = callApiToGetVoucherObj()
          var vOrderInfo = {
            menuCombo: vSelectedMenuStructure,
            loaiPizza: vLoaiPizza,
            drink: vSelectDrink,
            hoVaTen: vInpName,
            email: vInpEmail,
            dienThoai: vInpPhone,
            diaChi: vInpAddress,
            loiNhan: vInpMessage,
            voucher: vInpVoucher,
            phanTramGiamGia: vPercent,
            priceAnnualVND: function () {
              var vTotal = this.menuCombo.priceVND * (1 - this.phanTramGiamGia / 100);
              return vTotal;
            }
          };
         
        return vOrderInfo
    }
    // hàm Kiểm tra thông tin đơn hàng 
    function validateData(paramOrder){
        if(paramOrder.menuCombo == ""){
            alert("Hãy chọn Combo Pizza")
            return false
        } if(paramOrder.loaiPizza == ""){
            alert("Hãy chọn Loại Pizza")
            return false
        } if(paramOrder.drink == 0){
            alert("Hãy chọn Đồ Uống")
            return false
        } if(paramOrder.hoVaTen == ""){
            alert("Hãy nhập họ và tên")
            return false
        } if (isEmail(paramOrder.email) == false) {
            return false;
        }
        if (paramOrder.dienThoai === "") {
            alert("Hãy nhập số điện thoại!");
            return false;
        }
        if (isNaN(paramOrder.dienThoai)) {
            alert("Số điện thoại phải là dãy số!");
            return false;
        }
        if (paramOrder.diaChi === "") {
            alert("Hãy nhập địa chỉ!");
            return false;
        }
        return true;

    }

    //hàm kiểm tra email
    function isEmail(paramEmail) {
        if (paramEmail < 3) {
            alert("Nhập email...");
            return false;
        }
        if (paramEmail.indexOf("@") === -1) {
            alert("Email phải có ký tự @");
            return false;
        }
        if (paramEmail.startsWith("@") === true) {
            alert("Email không bắt đầu bằng @");
            return false;
        }
        if (paramEmail.endsWith("@") === true) {
            alert("Email không kết thúc bằng @");
            return false;
        }
        return true;
    }

    // hàm show thông tin đặt hàng vào modal
    function showOrderInfoToModal(paramOrderObj) {
        // var vVoucherObj = callApiToGetVoucherObj(paramOrderObj.maGiamGia);
        // if (vVoucherObj == null) {
        //     paramOrderObj.phanTramGiamGia = 0;
        // } else {
        //     paramOrderObj.phanTramGiamGia = vVoucherObj.phanTramGiamGia;
        // }
        $("#modal-inp-fullname").val(paramOrderObj.hoVaTen);
        $("#modal-inp-phone").val(paramOrderObj.dienThoai);
        $("#modal-inp-address").val(paramOrderObj.diaChi);
        $("#modal-inp-message").val(paramOrderObj.loiNhan);
        $("#modal-inp-voucherid").val(paramOrderObj.voucher);
        $("#inp-info-detail").html(
            "Xác nhận: " + paramOrderObj.hoVaTen + ", " + paramOrderObj.dienThoai + ", " + paramOrderObj.diaChi + "<br>"
            + "Menu " + paramOrderObj.menuCombo.menuName + ", Sườn nướng: " + paramOrderObj.menuCombo.suongNuong + ", Số lượng nước: " + paramOrderObj.menuCombo.drink + "<br>"
            + "Loại pizza: " + paramOrderObj.loaiPizza + ", Giá: " + paramOrderObj.menuCombo.priceVND + " vnd" + ", Mã giảm giá: " + paramOrderObj.voucher + "<br>"
            + "Phải thanh toán: " + paramOrderObj.priceAnnualVND() + " vnd ( giảm giá " + paramOrderObj.phanTramGiamGia + "%)"
        );
    }

    //hàm gọi api để check mã giảm giá
    function callApiToGetVoucherObj() {
        "use strict";
        var vVoucher = 0
        var vInpVoucher = $("#inp-voucher").val();
        if (vInpVoucher != "") {
            $.ajax({
                url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + vInpVoucher,
                type: "GET",
                async: false,
                success: function (paramRes) {
                    vVoucher = paramRes.phanTramGiamGia;
                },
                error: function () {
                     vVoucher = 0;
                }
            });  
        }
        return vVoucher 
        
    }
    // hàm call aPI CREATE ORDER
    function callApiCreateOrder(paramOjb){
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOjb),
            success: function (paramRes) {
                console.log(paramRes);
                // gOrderedObjRequest = paramRes;
                handleCreateOrderSuccess(paramRes);
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });

    }
    // hàm hiển thị modal khi tạo đơn hàng thành công
    function handleCreateOrderSuccess(paramOrdercode){
        $("#order-modal").modal("hide"); // ẩn modal order
        
        $("#result-order-modal").modal("show");
        $("#inp-modal-ordercode").val(paramOrdercode.orderCode)
        
    }
    // hàm đổi màu cho nút button combo
    function changeComboButtonColor(paramPlan) {
        var vBtnSmall = $("#btn-small"); // truy vấn nút chọn kích cỡ small
        var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
        var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large

        if (paramPlan === gCOMBO_SMALL) {
            vBtnSmall.prop({ class: "btn btn-success w-100", "data-is-selected-menu": "Y" });
            vBtnMedium.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
            vBtnLarge.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
        }
        else if (paramPlan === gCOMBO_MEDIUM) {
            vBtnSmall.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
            vBtnMedium.prop({ class: "btn btn-success w-100", "data-is-selected-menu": "Y" });
            vBtnLarge.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
        }

        else if (paramPlan === gCOMBO_LARGE) {
            vBtnSmall.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
            vBtnMedium.prop({ class: "btn btn-warning w-100", "data-is-selected-menu": "N" });
            vBtnLarge.prop({ class: "btn btn-success w-100", "data-is-selected-menu": "Y" });
        }
    }

    // hàm đổi màu cho nút button Loại Pizza
    function changeTypePizzaButtonColor(paramPizzaType) {
        var vBtnOcean = $("#btn-ocean");
        var vBtnHawai = $("#btn-hawai");
        var vBtnBacon = $("#btn-bacon");

        if (paramPizzaType === gPIZZA_OCEAN) {
            vBtnOcean.prop({ class: "btn btn-success w-100", "data-is-selected-pizza": "Y" });
            vBtnHawai.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
            vBtnBacon.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
        }
        else if (paramPizzaType === gPIZZA_HAWAI) {
            vBtnOcean.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
            vBtnHawai.prop({ class: "btn btn-success w-100", "data-is-selected-pizza": "Y" });
            vBtnBacon.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
        }

        else if (paramPizzaType === gPIZZA_BACON) {
            vBtnOcean.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
            vBtnHawai.prop({ class: "btn btn-warning w-100", "data-is-selected-pizza": "N" });
            vBtnBacon.prop({ class: "btn btn-success w-100", "data-is-selected-pizza": "Y" });
        }
    }
    // hàm load Data đồ uống
    function handleDrinkList(paramDrink) {
        $.each(paramDrink, function (i, item) {
            $("#select-drink").append($("<option>", {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }));
        });

    }



});